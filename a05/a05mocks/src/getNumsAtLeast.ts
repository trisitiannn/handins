import axios from 'axios';

const URL = 'https://example.com/api/v1/nums';
/* returns a JSON object containing an array of numbers between 0 an 9 inclusive
  of the form:
  { data: [ 1, 5, 6, 3, 9, 0 ...etc ] }
*/

interface ApiResponse {
  data: number[];
}

/** Reurns a Promise of an array of numbers from the service where only numbers
  greater than or equal to the provided minimum are kept.
  @param minNum - the array will not contain any numbers less than this
  @returns a Promise of an array of numbers where each number is greater than or
    equal to the provided minimum.
*/
export default async function getNumsAtLeast(
  minNum: number
): Promise<number[]> {
  const response = await axios.get(URL);
  return (response as ApiResponse).data.filter(n => n >= minNum);
}
