import getNumsAtLeast from './getNumsAtLeast';

import axios from 'axios';
jest.mock('axios');
const axiosMock = axios as jest.Mocked<typeof axios>;


describe('fake url', () =>{
  const expectedUrl = 'https://example.com/api/v1/nums';
  
  beforeEach(() => axiosMock.get.mockReset());
  beforeEach(() => axios.get(expectedUrl));
  
  test('make sure axios is calling', ()=>{
    expect(axiosMock.get).toHaveBeenCalledWith(expectedUrl);
    expect(axiosMock.get).not.toHaveBeenCalledWith('www.google.ca');
  });
  test('make sure values are working', async () =>{
    //axios.get(expectedUrl);
    const numberResponse = Promise.resolve({
      data: [1,2,3,4,5]
    })
    axiosMock.get.mockReturnValueOnce(numberResponse);
    const res = await getNumsAtLeast(2);
    expect(res).toStrictEqual([2,3,4,5]);
  });
  test('test edge case upperBound' , async () => {
    const numberResponse = Promise.resolve({
      data: [1,2,3,4,5]
    })
    axiosMock.get.mockReturnValueOnce(numberResponse);
    const res = await getNumsAtLeast(9);
    expect(res).toStrictEqual([]);
  })
  test('test edge case lowerBound' , async () => {
    const numberResponse = Promise.resolve({
      data: [1,2,3,4,5]
    })
    axiosMock.get.mockReturnValueOnce(numberResponse);
    const res = await getNumsAtLeast(-1);
    expect(res).toStrictEqual([1,2,3,4,5]);
  })


})

/*  TODO: test the getNumsAtLeast function by mocking the axios library

  You only need 2-4 tests
  1 to ensure that the function is calling the correct url
  1-3 to ensure that the filter is working (NOTE: the filter has 2 equiv. classes)

  Mock axios so that it returns controlled, testable values.
  NOTE: there is no service at https://example.com/api/v1/nums
    so you must mock axios for the tests to work.
  See https://gitlab.com/jhilliker/cpsc2350labdemos

  You may not alter the getNumsAtLeast.ts file.
  Submit only this file
*/
