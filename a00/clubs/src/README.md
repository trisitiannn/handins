
* This is an individual assignment.
* Do not modify {Club,Gender,Member}.ts
* You may not introduce any new dependencies.
* All files should lint with no errors.
* See the provided test skeletons for examples of constructor calls.
* Use ES6 style imports and exports, not ES5 or AMD.
* Use your source control system to backup your work.
* You must run `npm install` first to install the necessary node modules.

## A01

* Due: Oct 16 11:55 pm
* Implement the classes in Part1_Inheritance.ts
* Part1.skeleton.spec.ts should run without modification and without error.
  * This is not a complete test suite.
  * You do not have to write tests
  * Your classes must match the expected interface.
* Only 'Part1_Inheritance.ts' will be marked.
  * It will be run against the provided versions of the other files
  * and tested against a more extensive test suite.

## A02

* Due: Oct 16 11:55 pm
* Implement the classes in Part2_DIP.ts
* Part2.skeleton.spec.ts should run without modification and without error.
  * This is not a complete test suite.
  * You do not have to write tests
  * Your classes must match the expected interface.
* Only 'Part2_DIP.ts' will be marked.
  * It will be run against the provided versions of the other files
  * and tested against a more extensive test suite.

## A03

* Due: Oct 16 11:55 pm
* Create a UML Class Diagram for A01 & A02.
* Submit only: Part3_uml.{png,gif,jpg,svg,pdf,uxf}

I recommend using [UMLet](https://www.umlet.com/). It can be run without installation as long as a JVM is installed. You can run it off of the H drive in the labs.

There is also [UMLetino](http://www.umletino.com/), which is browser based, but I have not tried it.
