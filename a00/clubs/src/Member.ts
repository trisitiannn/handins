import Gender from './Gender';
export { Gender };

// DO NOT MODIFY THIS FILE

/** A potential member of a club. */
export default interface Member {
  /** The member's name. Each protion of the name is separated by space. First Middle Last. */
  readonly name: string;
  /** The member's gender. */
  readonly gender: Gender;
  /** The member's relationship status. */
  readonly isSingle: boolean;
  /** The member's age -- in years. */
  readonly age: number;
}

/** A simple club member */
export class MemberImpl implements Member {
  constructor(
    readonly name: string,
    readonly gender: Gender,
    readonly isSingle: boolean,
    readonly age: number
  ) {}
}
