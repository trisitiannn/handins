import Club, { Member } from './Club';
//import { MemberImpl, Gender } from './Member';
import {
  AbstractClub,
  LimitedSizeClub,
  NoHomerClub,
  BalancedGendersClub,
} from './Part1_Inheritance';

describe('A01', () => {
  let club: Club;

  beforeEach(() => {
    //set-up
  });

  describe('AbstractClub', () => {
    beforeEach(() => {
      club = new (class Always extends AbstractClub {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        canAdd(m: Member): boolean {
          return true;
        }
      })();
    });
    test('TODO', () => {
      expect(club.size).toBe(0);
    });
  });

  describe('LimitedSizeClub', () => {
    // tests
    test('TODO optional', () => {
      club = new LimitedSizeClub(0);
      expect(club.size).toBe(0);
    });
  });

  describe('NoHomerClub', () => {
    beforeEach(() => {
      club = new NoHomerClub();
    });

    test('TODO optional', () => {
      expect(club.size).toBe(0);
    });
  });

  describe('BalancedGendersClub', () => {
    beforeEach(() => {
      club = new BalancedGendersClub();
    });

    test('TODO optional', () => {
      expect(club.size).toBe(0);
    });

    test('TODO optional', () => {
      expect(club.size).toBe(0);
    });
  });
});
