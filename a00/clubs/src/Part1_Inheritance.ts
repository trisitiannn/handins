import Club, { Member, Gender } from './Club';

/* A01
  Implement the following classes in this file according to their documention:
    AbstractClub, LimitedSizeClub, NoHomerClub, BalancedGendersClub

  You may not use any of the code from Assignment 2.
  All classes in this module should be exported, and none of them should be a default export.

  You should only need 2 linter exceptions:
    *  for an explicit any when required for overloading
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    *  for an unused variable that was specified by the interface, but not used by the contract
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
*/

/** An abstract club missing only the `canAdd` method.
  Subclasses should override `canAdd`.
  @remarks
  `AbstractClub` is-a `Club`
  The class's constructor (if any) does not accept any parameters.
  The list of members must be kept `private`.
  You may not alter the public interface from `Club`.
  You may not implement the `canAdd` method -- leave it abstract.
  You must *overload* the `contains` method.
  No subclasses may maintain their own list of members.
*/
// TODO:  create AbstractClub class here
export abstract class AbstractClub implements Club {
  size = 0;
  protected members: Member[] = [];

  /**
   * @params newMembers, the new members,
   * Takes an array of Members and adds them to the list attached to the object,
   * always returns true but MUST BE OVERIDDEN IN SUB CLASSES BASED OF THEIR RESTRICTIONS
   */
  add(...newMembers: Member[]): boolean {
    for (const person of newMembers) {
      this.members.push(person); // you'll get a linter error here becuase it doesn't recognize member is an array?
      this.size++;
    }
    return true;
  }
  abstract canAdd(m: Member): boolean;

  contains(m: Member): boolean;
  contains(predicate: (m: Member) => boolean): boolean;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  contains(value: any): boolean {
    if (typeof value === 'function') {
      for (const person of this.members) {
        if (!value(person)) {
          return false;
        } else {
          return true;
        }
      }
    } else {
      return this.members.includes(value);
    }
    return false;
  }
  count(predicate: (m: Member) => boolean): number {
    let count = 0;
    for (const person of this.members) {
      if (predicate(person)) {
        count++;
      }
    }
    return count;
  }
}
// *** CLUBS ***

/** A club limited to a certain number of members.
  Once this number is reached, no other members are permitted to join.
  @remarks
  `LimitedSizeClub` is-a `AbstractClub`
  The class's constructor accepts a `number` that is the maximum number of club members.
  The `canAdd` method should optionally take a `Member` as its parameter.
*/
// TODO:  create LimitedSizeClub class here
export class LimitedSizeClub extends AbstractClub {
  private clubSize = 0;
  constructor(private readonly max: number) {
    super();
    this.clubSize = max;
  }
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public canAdd(m: Member): boolean {
    if (this.clubSize < this.size) {
      return false;
    } else {
      return true;
      this.size++;
    }
  }
}
/** A club that does not allow anyone whos name begins with "Homer" to join the club.
  @remarks
  `NoHomerClub` is-a `AbstractClub`
  The class's constructor (if any) does not accept any parameters.
*/
// TODO:  create NoHomerClub class here
export class NoHomerClub extends AbstractClub {
  public canAdd(m: Member): boolean {
    if (m.name.startsWith('homer')) {
      return false;
    } else {
      return true;
    }
  }
}
/** A club which keeps the number of male and female members within 1 of each other.
  ie: there can never be more than 1 male more than the number of females, and vice versa.
  @remarks
  `BalancedGendersClub` is-a `AbstractClub`
  The class's constructor (if any) does not accept any parameters.
  Override the `add` method to keep track of the number of men vs number of women in the club.
  Do not try to re-order members that wish to join. They are accepted or rejected in the order in which they apply.
*/
// TODO:  create BalancedGendersClub class here
export class BalancedGendersClub extends AbstractClub {
  gens = 0;
  public canAdd(m: Member): boolean {
    this.gens += m.gender;
    if (this.gens > 1 || this.gens < -1) {
      return false;
    } else {
      return true;
    }
  }
  add(...newMembers: Member[]): boolean {
    for (const person of newMembers) {
      if (this.canAdd(person)) {
        this.members.push(person); // you'll get a linter error here becuase it doesn't recognize member is an array?
        this.size++;
      } else {
        return false;
      }
    }
    return true;
  }
}
