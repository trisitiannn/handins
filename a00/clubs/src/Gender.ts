// DO NOT MODIFY THIS FILE

/** Genders of potential club members.
  For the simplicity of the assignment, there are only two genders.
*/
enum Gender {
  M = +1,
  F = -1,
}

export default Gender;
