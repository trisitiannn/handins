import Member, { Gender } from './Member';
export { Member, Gender };

// DO NOT MODIFY THIS FILE

/**
  A social club that has members.
  The club will enforce some membership rules when a new member attempts to join
  via the `add` method.
  Rules are checked by the `canAdd` method.
  A memeber will only be added if they pass the `canAdd` test, and if they are
  not already a member of the club.
*/
export default interface Club {
  /** The number of club members. */
  readonly size: number;

  /** Adds the given members to the club if they can join.
  Eligability is determined by the `canAdd` method,
  but members are never allowed to join the club if they are already a member.
  Members are added in the order that they arrive. Applicants are not reordered.
  @param m - the members who wish to join the club
  @returns true if the club's membership changes, false otherwise
  */
  add(...m: Member[]): boolean;

  /** Determines if the given member can join this club at this time.
  @param m - the member to examine for eligability
  @returns true if the member may join, false otherwise
  */
  canAdd(m: Member): boolean;

  /** Determines if the given member is a member of this club.
  @param m - the member to search for
  @returns true if the member is in the club, false otherwise
  */
  contains(m: Member): boolean;

  /**	Determines if the club contains any member that satisfies the given predicate.
  @param predicate - the test function to evaluate against each member
  @returns true if any of the club members satisfy the given predicate
  */
  contains(predicate: (m: Member) => boolean): boolean;

  /** Determines how many club members satisfy the given predicate.
  @param predicate - the test function to evaluate against each member
  @returns the count of how many members satisfy the given predicate
  */
  count(predicate: (m: Member) => boolean): number;
}
