import Club, { Member, Gender } from './Club';
import { AbstractClub } from './Part1_Inheritance';

/* A02
  Implement the following classes according to their documention:
    GeneralClub, ClubFactory
    ClubFactory should be the default export
    GeneralClub should be exported as GeneralClub.
  You may not use any of the code from Assignment 1 except where noted.

  You should only need 1 linter exception; for a necessary empty private constructor
  // eslint-disable-next-line @typescript-eslint/no-empty-function
*/

/**
  A general purpose club that uses *dependency injection* to determine if a given member may join the club.
  @remarks
  `GeneralClub` is-a `AbstractClub` from Assignment 1.
  You may not alter the public interface from `Club`.
  The constructor accepts (only) a predicate of the following type:
    `(m: Member, c: Club) => boolean`
    `m` - the member who wishes to join the club
    `c` - the club the member wishes to join. ie: this club
  You must implement the `canAdd` method to use the given predicate.
  You may not subclass this class.
  This class should be exported as `GeneralClub`.
*/
// TODO: create GeneralClub class here
export class GeneralClub extends AbstractClub {
  constructor(private prediacte: (m: Member, c: Club) => boolean) {
    super();
  }
  canAdd(m: Member): boolean {
    if (this.prediacte(m, this) && !this.contains(m)) {
      return true;
    } else {
      return false;
    }
  }
}
/** A factory for making clubs!
  @remarks
  `ClubFactory`
  This factory creates instances of `GeneralClub` by assigning an appropriate predicate.
  You may not modify a created `GeneralClub` in any way -- only by providing the necessary predicate.
  This is a factory class, so it should have no instances.
    Make the constructor private (and empty).
      You will have to disable the linter's error for that line.
    Determine the correct access modifier for its methods.
  This class should be the default export for this module.
  You may not use any of the clubs from Assignment 1.
*/
// TODO: create ClubFactory utility class here
export default class ClubFactory {
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  private constructor() {}
  /** Creates a club which only allows members who are
    18 years old or older.
    `adultsOnlyClub`
  */
  static adultsOnlyClub(): Club {
    return new GeneralClub((m: Member): boolean => m.age >= 18); // simple conditional returns based on if m is over 18
  }
  /** Creates a club which only allows members who are
    Female and single.
    `allTheSingleLadiesClub`
  */
  static allTheSingleLadiesClub(): Club {
    return new GeneralClub(
      (m: Member): boolean => m.gender === Gender.F && m.isSingle // simple conditional returns true if member is female and single
    );
  }
  /** Creates a club which only allows
    the given maximum number of members.
    `sizeLimitClub`
    @param limit - the maximum number of club members
  */
  static sizeLimitClub(limit: number): Club {
    return new GeneralClub((m: Member, c: Club): boolean => {
      // conditional checks if the size is smaller than the limit specified in the method call
      return c.size < limit;
    });
  }
  /** Creates a club which does not allow anyone whose
    name begins with "Homer" to join the club.
    `noHomerClub`
  */
  static noHomerClub(): Club {
    return new GeneralClub((m: Member): boolean => {
      if (m.name.startsWith('Homer')) {
        // the club checks to make sure noone has a name begining with Homer
        return false;
      }
      return true;
    });
  }
  /** Creates a club which allows at most one of it's member's
    names to begin with "Homer".
    ie: one nember may be named "Homer", but a second "Homer" is not allowed to join.
    `noHomersClub`
  */
  static noHomersClub(): Club {
    return new GeneralClub((m: Member, c: Club): boolean => {
      // conditions of doom!
      if (m.name.startsWith('Homer')) {
        // first check if the memebers name starts with homer
        if (
          c.contains((q: Member) => {
            // club defines a function contains which takes a predicate with the param of a member
            return !q.name.startsWith('Homer'); // that predicate returns true if there is not a name that starts with Homer
          })
        ) {
          return true; // if that returned true, return true
        } else {
          return false; // otherwise there is  a homer so return false
        }
      } else {
        return true; // otherwise the persons name does not start with home so return false
      }
    });
  }

  /** Creates a club which keeps the number of male and female members within 1 of each other.
    ie: there can never be more than 1 male more than the number of females, and vice versa.
    Do not try to reorder applicants. Apllicants are accepted or rejected in the order which they apply.
    Do not try to track state in the predicate.  Use the club's public accessors.
    `balancedGendersClub`
  */
  static balancedGendersClub(): Club {
    return new GeneralClub((m: Member, c: Club): boolean => {
      const males = c.count((male: Member): boolean => {
        // first tally up all the males using the count method in club and checks each member to see if they are male
        if (male.gender === Gender.M) {
          return true;
        } else {
          return false;
        }
      });
      const females = c.count((female: Member): boolean => {
        // does the same as above for females
        if (female.gender === Gender.F) {
          return true;
        } else {
          return false;
        }
      });
      const result = males - females; //subtracs the differences
      if (result == 0) {
        // result if they are even is to return true
        return true;
      } else if (
        (result < 0 && m.gender === Gender.F) ||
        (result > 0 && m.gender === Gender.M)
      ) {
        // result if they are breaking club rules
        return false;
      } else {
        // result for everything else
        return true;
      }
    });
  }
}
// constructor()

// adultsOnlyClub
// allTheSingleLadiesClub

// sizeLimitClub(limit: number)

// noHomerClub()

// noHomersClub()

// balancedGendersClub()
